#coding:utf-8  # Python3.X 源码文件默认使用utf-8编码，所以可以正常解析中文，无需指定 UTF-8 编码。
import matplotlib.pyplot as plt
import numpy as np
import math
import MySQLdb
import sys

# 打开数据库连接
db = MySQLdb.connect("localhost", "root", "Q12345678", charset='utf8' )
#获取连接的cursor，只有获取了cursor，我们才能进行各种操作
cur = db.cursor()
# cur.execute("create database up10 character set utf8") #print('创建up_10库完成')
cur.execute("use up10;") #print('进入test_db库完成')
#创建一个数据表 writers(id,name)
cur.execute("CREATE TABLE IF NOT EXISTS \
    info_main(Id INT PRIMARY KEY AUTO_INCREMENT, Code CHAR(6), Date_num INT, Date_mark INT, Date1 date)")
#以下插入了5条数据
cur.execute("INSERT INTO info_main(Code,Date_num,Date_mark,Date1) VALUES('666666',10,8,'2017-09-06')")
# 使用cursor()方法获取操作游标 



#查询info_main表中Date_num字段大于8的所有数据：
sql = "SELECT * FROM info_main \
       WHERE Date_num > '%d'" % (8)
try:
   # 执行SQL语句
   cur.execute(sql)
   # 获取所有记录列表
   results = cur.fetchall()
   for row in results:
      fname = row[0]
      lname = row[1]
      age = row[2]
      sex = row[3]
      date = row[4]
       # 打印结果
      print ("fname=%s,lname=%s,age=%d,sex=%s,date=%s" % \
             (fname, lname, age, sex,date))
except:
   print ("Error: unable to fetch data")
# 关闭数据库连接
db.close()



# 从[-1,1]中等距去50个数作为x的取值
class draw():

    def  __init__(self,n):
    	pass
    	self.max=n
    def dra(self):
        x = np.linspace(-1, 1, self.max)
        y = 2*x + 1
# 第一个是横坐标的值，第二个是纵坐标的值
        plt.plot(x, y)
# 必要方法，用于将设置好的figure对象显示出来
        plt.show()



class Circle:
    def cd(first, second):
        print (first*second)
    def __init__(self,radius): #圆的半径radius
        self.radius=radius

#以下实例声明了静态方法 area，前提是area函数不需要引用实例。类可以不用实例化就可以调用该方法 C.area()，当然也可以实例化后调用 C().area()。
    @staticmethod
    def area():
        print ("B")
         #计算面积

    @property
    def perimeter(self):
        return 2*math.pi*self.radius #计算周长
        # -*- coding:utf-8 -*-




# Circle.cd(1,2)
# c=Circle(10)

# Circle.area()
# print() #可以向访问数据属性一样去访问area,会触发一个函数的执行,动态计算出一个值
# print(c.perimeter) #将一个类的函数定义成特性以后，对象再去使用的时候c.perimeter,根本无法察觉自己的name是执行了一个函数然后计算出来的