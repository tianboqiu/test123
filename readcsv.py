#!/user/bin/env python
# -*-coding:utf-8-*-
# @time       : 16/11/8 12:09
# @Author     : Zhangxy
# @File       : 001baiduSearch.py
# @Software   : PyCharm
import pandas as pd
import pandas as pd
import numpy as np
import subprocess 
import time

df = pd.read_csv('/Users/TianBaiqiu/work/test123/NJ_his_dark.csv',index_col=0)
df=df.fillna('')
#df.drop(columns={'Conditions'}, inplace = True),nrows=2
# import subprocess 
# my_cmd = 'networksetup -listallhardwareports'#"sudo ifdown wlan0 && sudo ifup wlan0"   # might be wlan0
# proc = subprocess.Popen('networksetup -setairportpower en0 off', shell=True)
# time.sleep(30)
# proc = subprocess.Popen('networksetup -setairportpower en0 on', shell=True)


class BasisCal(object):

    '''
    基础数据计算
    '''
    # 计算焓值
    @staticmethod
    def enthalpy(self, df):
        
        #  cal enthalpy by temp and humidity
        df.dropna(inplace=True)
        T_pf = df['temp']
        H    = df['hum']
        kelwen = T_pf + 273.15
        saturated_steam = np.e**(-5800.2206/kelwen+1.3914993-0.04860239*kelwen+0.0000417648*kelwen**2-0.0000000144521*kelwen**3+6.5459673*np.log(kelwen))
        steam_partial_pressure = H*saturated_steam/100
        moisture_content = 622*steam_partial_pressure/(101325-steam_partial_pressure)
        h = 1.01*T_pf + 0.001*moisture_content*(2501+1.84*T_pf)
        return h

    # 计算湿球温度
    def wet_bulb_temperature(self,weather_df):
        """
        Parameter:
            weather_df: pandas.DataFrame

        Return:
            pandas.Series

        Example:
        >> weather_df
                                    TemperatureC  Humidity
        2016-10-01 00:00:00+08:00         36         30
        2016-10-01 00:05:00+08:00         36         55
        >> Utility.wet_bulb_temperature(weather_df)
                                   WetBulbTemperature
        2016-10-01 00:00:00+08:00          3
        2016-10-01 00:05:00+08:00          3
        """
        index = weather_df.index
        df    = weather_df.dropna()
        cols  = [i for i in df.columns if "temp" in i]
        cols.extend([i for i in df.columns if "hum" in i])
        if len(cols) != 2: return pd.DataFrame()
        wets  = [self.wet_bulb_temperature_interpolate(item) for item in df[cols].values]

        return pd.DataFrame(wets, index=df.index, columns=["WetBulbTemperature"]).reindex(index)

    def wet_bulb_temperature_interpolate(self,item):

        hum = item[1]/100.0

        table = [[-5.082366, -4.740581, 0.750256, 0.811202, 0.3, 0.4],
                 [-4.740581, -4.131947, 0.811202, 0.858568, 0.4, 0.5],
                 [-4.131947, -3.342766, 0.858568, 0.896106, 0.5, 0.6],
                 [-3.342766, -2.536432, 0.896106, 0.928161, 0.6, 0.7],
                 [-2.536432, -1.666897, 0.928161, 0.955048, 0.7, 0.8],
                 [-1.666897, -0.824302, 0.955048, 0.978813, 0.8, 0.9],
                 [-0.824302, 0.0, 0.978813, 1.0, 0.9, 1.0]]

        for params in table[:-1]:
            if hum >= params[4] and hum < params[5]:
                c = params[0] + (hum-params[4]) * ((params[1]-params[0])/(params[5]-params[4]))
                d = params[2] + (hum-params[4]) * ((params[3]-params[2])/(params[5]-params[4]))

                return c*hum + d*item[0]
        else:
            c = table[-1][0] + (hum-table[-1][4]) * ((table[-1][1]-table[-1][0])/(table[-1][5]-table[-1][4]))
            d = table[-1][2] + (hum-table[-1][4]) * ((table[-1][3]-table[-1][2])/(table[-1][5]-table[-1][4]))

            return c*hum + d*item[0]

print(BasisCal().wet_bulb_temperature(df))
#作为示例，输出CSV文件的前5行和最后5行，这是pandas默认的输出5行，可以根据需要自己设定输出几行的值
'''
from selenium import webdriver
driver = webdriver.Chrome()
driver.get("http://www.baidu.com")
driver.find_element_by_id('kw').send_keys('selenium')
driver.find_element_by_id('su').click()
driver.quit()
#速度最快

reader = pd.read_csv('/Users/TianBaiqiu/work/test123/NJ_his_dark.csv',iterator=True,sep=';')
loop = True
chunksize = 100000
chunks = []
while loop:
    try:
        chunk = reader.get_chunk(chunksize)
        chunk = chunk.dropna(axis=1)
        chunk = chunk.drop_duplicates()
        chunks.append(chunk)
    except StopIteration:
        loop = False
df = pd.concat(chunks,ignore_index=True)
df = df.dropna(axis=1)
df = df.drop_duplicates()
print (df)
'''

