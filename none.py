#!/usr/bin/env python
import test

print (dir(test))


import sys
print(sys.path)

import pandas as pd
import pandas as pd
import numpy as np
import readcsv
def read_csv(fname):

    raw_df = pd.read_csv(fname)
    raw_df.columns = ["time"]+list(raw_df.columns)[1:]
    index  = pd.to_datetime(raw_df["time"], utc=True)
    raw_df.index = index

    df = raw_df.drop(["time"], axis=1)
    df.index = df.index.tz_convert("Asia/Shanghai")
    return df
df = read_csv('/Users/TianBaiqiu/work/test123/NJ_his_dark.csv')
df.drop(columns={'Conditions'}, inplace = True)
print(readcsv.BasisCal().wet_bulb_temperature(df))
# test.draw(50).dra() 当使用from sound.effects.echo import echofilter时，这种方法会导入子模块: echo，并且可以直接使用他的 echofilter() 函数:
'''

def funcA(A):
    print("function A")

def funcB(B):
    print(B(2))
    print("function B")

@funcA
@funcB
def func(c):
    print("function C")  
    return c**2
#则整个程序的执行过程就是funA(funB(funC))


print ("你好，世界")

list=[1,2,3,4]
it=iter(list)
for x in it:
    print (x,end=" ")
print (str(list[1])+'tian') #'%d' %  将数字型转变为字符串
if ( 1 in list ):
    print ("1 - 变量 a 在给定的列表中 list 中")
else:
    print ("1 - 变量 a 不在给定的列表中 list 中")

import re
print(re.match('www', 'www.runoob.com').span())  # 在起始位置匹配
print(re.match('com', 'www.runoob.com'))         # 不在起始位置匹配


def change(a):
    a=[10,100]
    b=100

a=[1,2,3,4]
change (a)
print (a[1])
for i in reversed(a): #要反向遍历一个序列，首先指定这个序列，然后调用 reversed() 函数：
    print(i)
for i in sorted(a): #要按顺序遍历一个序列，使用 sorted() 函数返回一个已排序的序列，并不修改原值：
    print(i)
knights = {'gallahad': 'the pure', 'robin': 'the brave'}
for k, d in knights.items():  #关键字和对应的值可以使用 items() 方法同时解读出来：ß
    print(k, d)

for i, v in enumerate(knights): #索引位置和对应值可以使用 enumerate() 函数同时得到：
    print(i, v)
a
a.append(5)
print ([x**2 for x in a])
'''
class foo:
    def hello(self,name):
            print ('i m' + name)

    def timerange(self,ctime):
        print("df")
        self.hello("hei")
        config="13:15:00~18:24:00,6:00:00~9:00:00"
        for i in config.split(","):
            if int(ctime.replace(':',''))>=int(i.split("~")[0].replace(':',''))\
            and int(ctime.replace(':',''))<int(i.split("~")[1].replace(':','')):
                return True
#obj = foo()
if (foo().timerange("5:00:01")):
    print("dd")
    #print (foo().timerange("14:00:01"))
config="13:15:00~18:24:00,6:00:00~9:00:00"

'''
class Car:
    def __init__(self, c, b, m):
        "这是构造方法"
        print("__init__被调用!")
        self.color, self.brand, self.model = c, b, m

    def run(self, speed):
        print(self.color, "的", self.brand,
              self.model, "正在以", speed,
              "公里/小时的速度驶")

    def change_color(self, c):
        "此方法用来改变颜色"
        self.color = c  # 换色


a4 = Car("红色", "奥迪", "A4")

a4.run(199)
a4.change_color("黑色")  # 推荐
# a4.color = "银色"  # 不推荐
a4.run(233)

ts = Car("蓝色", "Tesla", "S")
ts.run(300)
'''